from django.db import models

class Blog(models.Model):
    title = models.CharField(max_length=150)
    date = models.DateField()
    discription = models.TextField()